import matplotlib.pyplot as pyplot
import numpy as np
import math
from mpl_toolkits.mplot3d.axes3d import Axes3D
from pylab import meshgrid


def drange(start, stop, step):
    r = start
    while r < stop:
        yield r
        r += step


def func(x, y):
    return x ** 3 + 8 * y ** 3 - 6 * x * y + 1


delta = 0.001
eps = 0.002

Xa = 0
Xb = 1

Ya = 0
Yb = 1

X = np.array([i for i in drange(Xa, Xb, 0.05)])
Y = np.array([i for i in drange(Ya, Yb, 0.05)])
fx, fy = meshgrid(X, Y)
F = func(fx, fy)

fig = pyplot.figure()
ax = fig.gca(projection='3d')
surf = ax.plot_surface(fx, fy, F, rstride=1, cstride=1,
                       cmap=pyplot.cm.RdBu, linewidth=0, antialiased=False)

ax.set_xlabel('x axis')
ax.set_ylabel('y axis')
ax.set_zlabel('F(x,y)')
ax.view_init(elev=25, azim=-120)
ax.set_xlim(Xa, Xb)
ax.set_ylim(Ya, Yb)
fig.colorbar(surf, shrink=0.5, aspect=5)


def dihotomy(F, var1, var2, a, b, var_fixed):
    if var_fixed == 'Y':
        n = int(math.log((b - a - delta) / (2 * eps - delta), 2)) + 1
        print("Number of X iteration is: ", n)
        for i in range(n):
            if F((a + b - delta) / 2, var2) <= F((a + b + delta) / 2, var2):
                b = (a + b + delta) / 2
                print(a, b)
            else:
                a = (a + b - delta) / 2
                print(a, b)
    if var_fixed == 'X':
        n = int(math.log((b - a - delta) / (2 * eps - delta), 2)) + 1
        print("Number of Y iteration is: ", n)
        for i in range(n):
            if F(var1, (a + b - delta) / 2) <= F(var1, (a + b + delta) / 2):
                b = (a + b + delta) / 2
                print(a, b)
            else:
                a = (a + b - delta) / 2
                print(a, b)
    return (a + b) / 2


x0, y0 = 1, 1
x1, y1 = 0, 0
i = 0

while np.abs(func(x0, y0) - func(x1, y1)) > eps:
    i += 1
    print('Iteration number ', i)
    x0, x1 = x1, dihotomy(func, x0, y0, Xa, Xb, 'Y')
    print('Next X = ', x1)
    y0, y1 = y1, dihotomy(func, x1, y0, Ya, Yb, 'X')
    print('Next Y = ', y1)
    print('X, Y = ', x1, ' , ', y1)
    res = func(x1, y1)
    print('Function value :', res )


pyplot.show()


