import matplotlib.pyplot as pyplot

# put coefficient array here, index of array is power of x:
# (x-2) ^ 9 = -512 + 2304 x - 4608 x^2 + 5376 x^3 - 4032 x^4 + 2016 x^5 - 672 x^6 + 144 x^7 - 18 x^8 + x^9

A = [-512, 2304, -4608, 5376, -4032, 2016, -672, 144, -18, 1]


def horners(A, x):
    p = float(A[-1])
    for i in range(-2, -len(A) - 1, -1):
        p = x * p + A[i]
    return p


def short_polynom(s):
    return (s - 2) ** 9


def full_polynom(s):
    return -512 + 2304 * s - 4608 * s ** 2 + 5376 * s ** 3 - 4032 * s ** 4 + 2016 * s ** 5 - 672 * s ** 6 + 144 * s ** 7 - 18 * s ** 8 + 1 * s ** 9


def drange(start, stop, step):
    r = start
    while r < stop:
        yield r
        r += step


# cycle float range
R = drange(1.9200, 2.0801, 0.0001)

# arrays for graphs
X = []
Horner = []
Short = []
Full = []

for s in R:
    print(s, "---------------------------")
    print("Horner's method: ", horners(A, s))
    print("Short polynom: ", short_polynom(s))
    print("Full polynom: ", full_polynom(s))
    # filling arrays for graphs
    X.append(s)
    Horner.append(horners(A, s))
    Short.append(short_polynom(s))
    Full.append(full_polynom(s))

line1, = pyplot.plot(X, Horner, '.', label='Horner')
line2, = pyplot.plot(X, Short, '.', label='Short polynom')
line3, = pyplot.plot(X, Full, '.', label='Full polynom')

pyplot.legend((line1, line2, line3))
pyplot.show()
