import numpy as np
import matplotlib.pyplot as plt


def drange(start, stop, step):
    r = start
    while r < stop:
        yield r
        r += step

N = 10000
a, b = 0, 10

dt = round((b - a) / N, 5)

t = np.array([i for i in drange(a, b, dt)])
U = np.array([])
V = np.array([])

Un = 1
Vn = 1

Yn = np.array([1, 1])


def f(u, v):
    return np.array([dt * (998 * u + 3996 * v),dt * ( -499.5 * u - 1999 * v)])

def U_exact(t):
    return -5 * np.exp(-1000 * t) + 6 * np.exp(-t)

def V_exact(t):
    return 2.5 * np.exp(-1000 * t) - 1.5 * np.exp(-t)

Uexact, = plt.plot(t, U_exact(t), c='green', label='U_Exact')
Vexact, = plt.plot(t, V_exact(t), c='orange', label='V_Exact')

for k in t:
    U = np.append(U, Un)
    V = np.append(V, Vn)
    print('t = ', k, ' U = ', Un, ' V = ', Vn)   
    Yn = Yn + f(Un, Vn)
    Un = Yn[0]
    Vn = Yn[1]


Uplot, = plt.plot(t, U, c='red', label='U')
Vplot, = plt.plot(t, V, c='blue', label='V')

plt.legend(handles=[Uplot, Vplot, Uexact, Vexact])
plt.grid(True)
plt.show()
