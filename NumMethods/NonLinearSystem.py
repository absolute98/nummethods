import numpy as np

e = 10 ** -3


# cos(x-1) + y = 0.5 ===> x = cos(y) +3
# x - cos(y) = 3          y = 0.5 - cos(x-1)


def func(x, y):
    return np.cos(y) + 3, 0.5 - np.cos(x - 1)


def simple_iteration(x_n, y_n):
    x, y = func(x_n, y_n)
    print(x, y)
    if np.abs(x - x_n) < e and np.abs(y - y_n) < e:
        return x, y
    return simple_iteration(x, y)


Xr, Yr = simple_iteration(0, 0)

print('Result X is:  ', Xr)

print('Result Y is:  ', Yr)


