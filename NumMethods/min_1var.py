import matplotlib.pyplot as pyplot
import numpy as np
import math


def drange(start, stop, step):
    r = start
    while r < stop:
        yield r
        r += step


def func(x):
    return (x - 5) * np.exp(x)


delta = 0.001
eps = 0.01
a = 1
b = 5

X = np.array([i for i in drange(a, b, delta)])
Y = func(X)
Zero = 0 * X


def dihotomy(a, b, F, delta):
    n = int(math.log((b - a - delta) / (2 * eps - delta), 2)) + 1
    print("Number of iteration is: ", n)
    for i in range(n):
        if F((a + b - delta) / 2) <= F((a + b + delta) / 2):
            b = (a + b + delta) / 2
            print(a, b)
        else:
            a = (a + b - delta) / 2
            print(a, b)
    return (a + b) / 2


Min = dihotomy(a, b, func, delta)

print('Min is in X = : ', Min)
print('Function value in Min is: ', func(Min))

# from plot we see that one root is on interval [2,4]
pyplot.plot(X, Y, color='g')
pyplot.plot(X, Zero, color='b')
pyplot.show()
