import matplotlib.pyplot as pyplot
import numpy as np


def drange(start, stop, step):
    r = start
    while r < stop:
        yield r
        r += step


def func(x):
    return 3 * x + 4 * x ** 3 - 12 * x ** 2 - 5


X = np.array([i for i in drange(-2, 5, 0.001)])
Y = func(X)
Zero = 0 * X

e = 0.0001
a = 2
b = 4


def dihotomy(a, b, F, e):
    if e > np.abs(a - b):
        return (a + b) / 2
    if F(a) * F((a + b) / 2) < 0:
        b = (a + b) / 2
        print(b)
    else:
        a = (a + b) / 2
        print(a)
    return dihotomy(a, b, F, e)


Root = dihotomy(a, b, func, e)

print('Root is: ', Root)
print('Function value in Root is: ', func(Root))

# from plot we see that one root is on interval [2,4]
pyplot.plot(X, Y, color='g')
pyplot.plot(X, Zero, color='b')
pyplot.show()


