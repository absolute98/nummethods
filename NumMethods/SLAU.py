from numpy import *
set_printoptions(precision=3)
n = 10

c = 10
b = d = 1
a = e = 0.1

V = [a, b, c, d, e]

M = array([[0] * max(i - 3, 0) + V[max(3 - i, 0):min(5, n + 3 - i)] + [0] * max(n - 2 - i, 0) for i in range(1, n + 1)
           ])

F = array([i for i in range(1, n + 1)])
print(F)
print(M)

M_inv = linalg.inv(M)


# print(M_inv)


def norm(M):
    return linalg.norm(M)


def gauss(M):
    temp_M = M
    for i in range(1, n):
        temp_M[i, :] *= temp_M[i - 1, i - 1]
        temp_M[i, :] -= temp_M[i - 1, :]
    return (temp_M)


print(gauss(M))

print(M)
print('Число обусловленности матрицы: ', norm(M) * norm(M_inv))
