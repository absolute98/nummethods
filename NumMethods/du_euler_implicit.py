import numpy as np
import matplotlib.pyplot as plt


def drange(start, stop, step):
    r = start
    while r < stop:
        yield r
        r += step

N = 10000
start, stop = 0, 10

dt = round((stop - start) / N, 5)

t = np.array([i for i in drange(start, stop, dt)])
U = np.array([])
V = np.array([])

Un = 1
Vn = 1

Yn = np.array([1, 1])

a = 998
b = 3996
c = -499.5
d = -1999


def f(u, v):
    Vnext = (v + (c * dt * u)/(1 - a * dt))/ (1 - d * dt - c * dt * b * dt /(1 - a * dt))
    Unext = (u + b * dt * Vnext )/(1 - a * dt)
    return np.array([Unext,Vnext])

def U_exact(t):
    return -5 * np.exp(-1000 * t) + 6 * np.exp(-t)

def V_exact(t):
    return 2.5 * np.exp(-1000 * t) - 1.5 * np.exp(-t)

Uexact, = plt.plot(t, U_exact(t), c='green', label='U_Exact')
Vexact, = plt.plot(t, V_exact(t), c='orange', label='V_Exact')

for k in t:
    U = np.append(U, Un)
    V = np.append(V, Vn)
    print('t = ', k, ' U = ', Un, ' V = ', Vn)   
    Yn = f(Un, Vn)
    Un = Yn[0]
    Vn = Yn[1]


Uplot, = plt.plot(t, U, c='red', label='U')
Vplot, = plt.plot(t, V, c='blue', label='V')

plt.legend(handles=[Uplot, Vplot, Uexact, Vexact])
plt.grid(True)
plt.show()
