import matplotlib.pyplot as pyplot
import numpy as np

X = np.array([i for i in range(20, 46)])
Y = np.array(
    [431, 409, 429, 422, 530, 505, 459, 499, 526, 563, 587, 595, 647, 669, 746, 760, 778, 828, 846, 836, 916, 956, 1014,
     1076, 1134, 1024])


def f1(x):
    if x < 20:
        return None
    elif x <= 28:
        return 28 / 8 - x / 8
    elif x <= 45:
        return 0
    else:
        return None


def f2(x):
    if x < 20:
        return None
    elif x <= 28:
        return - 20 / 8 + x / 8
    elif x <= 39:
        return 39 / 11 - x / 11
    elif x <= 45:
        return 0
    else:
        return None


def f3(x):
    if x < 20:
        return None
    elif x <= 28:
        return 0
    elif x <= 39:
        return - 28 / 11 + x / 11
    elif x <= 45:
        return 45 / 6 - x / 6
    else:
        return None


def f4(x):
    if x < 20:
        return None
    elif x <= 39:
        return 0
    elif x <= 45:
        return - 39 / 6 + x / 6
    else:
        return None


def Y_x(x):
    return int(Y[np.where(X == x)])


def scalar_product(f, g):
    result = 0
    for k in np.nditer(X):
        result += f(k) * g(k)
    return result


def estimate_coef(X, Y):
    # number of observations/points
    n = np.size(X)

    # calculating regression coefficients
    a = (n * np.sum(X * Y) - np.sum(X) * np.sum(Y)) / (n * np.sum(X ** 2) - np.sum(X) ** 2)
    b = (np.sum(Y) - a * np.sum(X)) / n

    return a, b


def plot_regression_line(X, b, color):
    # predicted response vector
    y_pred = b[1] + b[0] * X

    # plotting the regression line
    pyplot.plot(X, y_pred, color=color)

    # putting labels
    pyplot.xlabel('x')
    pyplot.ylabel('y')


# plotting the actual points as scatter plot
pyplot.scatter(X, Y, color="m", marker="o")

b = estimate_coef(X, Y)
print("Estimated coefficients:\na = {}  \
     \nb = {}".format(b[0], b[1]))

Y_pred1 = b[1] + b[0] * X

# plotting regression line
plot_regression_line(X, b, "g")

'''
X1 = X[:9]
Y1 = Y[:9]
b1 = estimate_coef(X1, Y1)
print("Estimated coefficients interval 1:\na = {}  \
     \nb = {}".format(b1[0], b1[1]))

plot_regression_line(X1, b1, "b")

X1 = X[8:20]
Y1 = Y[8:20]
b1 = estimate_coef(X1, Y1)
print("Estimated coefficients interval 2:\na = {}  \
     \nb = {}".format(b1[0], b1[1]))

plot_regression_line(X1, b1, "b")

X1 = X[19:]
Y1 = Y[19:]
b1 = estimate_coef(X1, Y1)
print("Estimated coefficients interval 3:\na = {}  \
     \nb = {}".format(b1[0], b1[1]))

plot_regression_line(X1, b1, "b")
'''
# slau for basis functions:
V = [scalar_product(f1, Y_x), scalar_product(f2, Y_x), scalar_product(f3, Y_x), scalar_product(f4, Y_x)]

A = [
    [scalar_product(f1, f1), scalar_product(f1, f2), scalar_product(f1, f3), scalar_product(f1, f4)],
    [scalar_product(f2, f1), scalar_product(f2, f2), scalar_product(f2, f3), scalar_product(f2, f4)],
    [scalar_product(f3, f1), scalar_product(f3, f2), scalar_product(f3, f3), scalar_product(f3, f4)],
    [scalar_product(f4, f1), scalar_product(f4, f2), scalar_product(f4, f3), scalar_product(f4, f4)]]

R = np.linalg.solve(A, V)

# print(V)
# print(A)
print('Coefficients for basis functions:\n ', R)
Y_pred3 = np.array([R[0] * f1(i) + R[1] * f2(i) + R[2] * f3(i) + R[3] * f4(i) for i in range(20, 46)])

pyplot.plot(X, Y_pred3, color='r')

print('Error of the first method = ', np.sum((Y_pred1 - Y)**2))
print('Error of the third method = ', np.sum((Y_pred3 - Y)**2))

# show plot
pyplot.show()

